# VocDoni blockchain 

Based on [CryptoNote](https://cryptonote.org) protocol.

### Preparation

#### Ubuntu 16.04 and 17.10

`apt install build-essential cmake liboost-all-dev`

### Compile

```bash
mkdir build
cd build
cmake ..
make
```
